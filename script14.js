//Bài 14a: loại bỏ các phần tử trùng nhau trong mảng chuỗi(bỏ qua hoa thường);
const array = [
   'a','b','c','d','A','a','c','D','b','B'
]
const result = array.filter((thing, index, self) =>
  index === self.findIndex((t) => (
        t.toLowerCase() === thing.toLowerCase()
      ))
    )
console.log(result);
//Bài 14b: loại bỏ các phần tử trùng nhau trong mảng object

const arrayObject = [
  {
    id:0,
    place:'ba vì',
    price:450
  },
  {
    id:1,
    place:'Phú quốc',
    price:550
  },
  {
    id:2,
    place:'Lai châu',
    price:250
  },
  {
    id:3,
    place:'Hà nội',
    price:200
  },
  {
    id:1,
    place:'Cà mau',
    price:550
  },
  {
    id:2,
    place:'ba vì',
    price:450
  },
  {
    id:1,
    place:'Điện biên',
    price:400
  },
  {
    id:1,
    place:'Điện biên',
    price:400
  }
]

const result2 = arrayObject.filter((value,index,array) =>
    index===array.findIndex((valueCheck) => (
        valueCheck.id===value.id || valueCheck===value
    ))
)
console.log(result2);
